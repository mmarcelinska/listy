#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "moduly.h"

int main()
{
    int choice = 0;
    char surname[20] = "";

    // naglowek listy
    struct Client *head;
    head= (struct Client*)malloc(sizeof(struct Client));
    strcpy(head->surname, "Kowalski");
    head->next = NULL;

    while(1)
    {
        //Wybor dzialania programu
        printf("\n\nProgram Listy 1.0\n Co chcesz zrobic?\n 1 - dodaj nowego klienta \n 2 - usun klienta po nazwisku \n 3 - wyswietl ilosc klientow \n 4 - wyswietl nazwiska klientow \n 5 - zakoncz dzialanie programu \n Twoj wybor: ");
        scanf("%d", &choice);

    //Test
    //    printf("%d \n", choice);

        switch(choice)
        {
            case 1: // dodanie nowego klienta na koniec listy
            printf("\nPodaj nazwisko nowego klienta: ");
            scanf("%s", surname);
            push_back(&head, surname);
            break;

            case 2: //usuwanie klienta o danym nazwisku
            printf("\nPodaj nazwisko klienta ktorego chcesz usunac: ");
            scanf("%s", surname);
            pop_by_surname(&head, surname);
            break;

            case 3:
            printf("\nLiczba klientow wynosi: %d ", list_size(head));
            break;


            case 4:
            show_list(head);
            break;

            case 5:
            free(head);
            exit(1);
            break;
        
            default:
            break;
        }
    }

    free(head);
    head=NULL;

    return 0;
}
